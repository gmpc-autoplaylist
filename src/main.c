/* gmpc-autoplaylist (GMPC plugin)
 * Copyright (C) 2006-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <stdio.h>
#include <string.h>
#include <glade/glade.h>
#include <libmpd/libmpd.h>
#include <libmpd/debug_printf.h>
#include <gmpc/plugin.h>
#include <config.h>
GladeXML *apl_xml = NULL;
int  e_util_utf8_strstrcase (const gchar *haystack, const gchar *needle);
int apl_right_mouse_menu(GtkWidget *menu, int type, GtkWidget *tree, GdkEventButton *event);


int apl_get_enabled();
void apl_set_enabled(int enabled);

/** in gmpc */
void pl3_option_menu_activate(); 

gmpcPlBrowserPlugin apl_gpb  = {
	.cat_right_mouse_menu = apl_right_mouse_menu
};
int plugin_api_version = PLUGIN_API_VERSION;
gmpcPlugin plugin = {
	.name           = "Automatic Playlist Creation",
	.version        = {PLUGIN_MAJOR_VERSION,PLUGIN_MINOR_VERSION,PLUGIN_MICRO_VERSION},
	.plugin_type    = GMPC_PLUGIN_PL_BROWSER,
	.browser        = &apl_gpb, /* browser intergration */
	.get_enabled    = apl_get_enabled,
	.set_enabled    = apl_set_enabled
};

int apl_get_enabled()
{
	return cfg_get_single_value_as_int_with_default(config, "autoplaylist", "enable", TRUE);
}
void apl_set_enabled(int enabled)
{
	cfg_set_single_value_as_int(config, "autoplaylist", "enable", enabled);
	pl3_option_menu_activate(); 
}

static void apl_close(GtkWidget *dialog, GladeXML *apl_xml)
{
	GtkWidget *tree = glade_xml_get_widget(apl_xml, "result_tree");
	GtkListStore *model = (GtkListStore *)gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	gtk_widget_destroy(GTK_WIDGET(dialog));
	g_object_unref(model);
	g_object_unref(apl_xml);
}

static int apl_data_matched(MpdData *data, int field, int rule, char *match)
{
	int matched = 0;
	gchar *key = NULL;
	/* get the correct field */
	if(field == MPD_TAG_ITEM_ARTIST && data->song->artist) key = data->song->artist;
	else if(field == MPD_TAG_ITEM_ALBUM && data->song->album) key = data->song->album;
	else if(field == MPD_TAG_ITEM_TITLE && data->song->title) key = data->song->title;
	else if(field == MPD_TAG_ITEM_TRACK && data->song->track) key = data->song->track;
	else if(field == MPD_TAG_ITEM_NAME && data->song->name) key = data->song->name;
	else if(field == MPD_TAG_ITEM_GENRE && data->song->genre) key = data->song->genre;
	else if(field == MPD_TAG_ITEM_DATE && data->song->date) key = data->song->date;
	else if(field == MPD_TAG_ITEM_COMPOSER && data->song->composer) key = data->song->composer;
	else if(field == MPD_TAG_ITEM_PERFORMER&& data->song->performer) key = data->song->performer;
    else if(field == MPD_TAG_ITEM_DISC && data->song->disc) key = data->song->disc;
	else key = data->song->file;
	if(key == NULL)
	{
		return FALSE;
	}
	if(rule == 0)	matched = e_util_utf8_strstrcase (key,match);
	else if(rule == 1) matched = !e_util_utf8_strstrcase (key, match);
	else matched = g_utf8_collate(match,key)?FALSE:TRUE;
	/* return value */
	return matched;
}


static MpdData * apl_data_filter(MpdData *data, int field, int rule, char *match)
{
	while(data)
	{
		if(apl_data_matched(data, field, rule, match) == 0)
		{

			data = mpd_data_delete_item(data);
		}
		else {
			if(!mpd_data_is_last(data))
			{
				data = mpd_data_get_next(data);
			}
			else
			{
				return mpd_data_get_first(data);
			}
		}
	}
	return NULL;
}

static void apl_data_filter_itemwise(MpdData **total_list, int field, int rule, char *match)
{
	GtkWidget *tree = glade_xml_get_widget(apl_xml, "result_tree");
	GtkListStore *model =(GtkListStore *) gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	while(*total_list)
	{
		if(apl_data_matched(*total_list, field, rule, match))
		{
			GtkTreeIter iter;
			gtk_list_store_append(model, &iter);
			gtk_list_store_set(model, &iter,
					0, (*total_list)->song->title,
					1,(*total_list)->song->artist,
					2,(*total_list)->song->album,
					3,(*total_list)->song->file,-1);
			*total_list = mpd_data_delete_item(*total_list);
		}
		else
		{
			if(!mpd_data_is_last(*total_list))
			{
				*total_list = mpd_data_get_next(*total_list);
			}
			else
			{
				*total_list = mpd_data_get_first(*total_list);
				return;
			}
		}
	}
	*total_list = mpd_data_get_first(*total_list);
}

static void apl_search()
{
	GtkWidget *tree = glade_xml_get_widget(apl_xml, "result_tree");
	GtkListStore *rs_store =(GtkListStore *) gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
	
	GtkTreeModel *model = gtk_tree_view_get_model
		(GTK_TREE_VIEW (glade_xml_get_widget(apl_xml, "apl_tree")));
	int match_type = gtk_toggle_button_get_active
		(GTK_TOGGLE_BUTTON(glade_xml_get_widget(apl_xml, "ck_any_item")));
	MpdData *data = mpd_database_get_complete(connection);
	/** clear previous results */
	gtk_list_store_clear(rs_store);
    if(data != NULL)
    {
		GtkTreeIter iter;
		if(gtk_tree_model_get_iter_first(model, &iter))
		{
			do{
				gint field, rule;
				gchar *cfield, *crule, *match;
				gtk_tree_model_get(model, &iter,
						0, &cfield,
						1, &crule,
						2, &match,
						-1);

				field = mpd_misc_get_tag_by_name (cfield);
				if(!strcmp(crule, "Contains")) rule = 0;
				else if (!strcmp(crule, "Does not contain")) rule = 1;
				else rule = 2;
				if(match_type)
				{
					apl_data_filter_itemwise(&data, field, rule, match);
				}
				else
				{
					data = apl_data_filter(data, field, rule, match);
				}
				g_free(cfield);
				g_free(crule);
				g_free(match);
			}while(gtk_tree_model_iter_next(model, &iter));
		}
		if(match_type)
		{
			mpd_data_free(data);
			data = NULL;
		}
		for(;data; data = mpd_data_get_next(data))
		{
			GtkTreeIter iter;
			gtk_list_store_append(rs_store,&iter);
			gtk_list_store_set(rs_store, &iter,
					0, data->song->title,
					1, data->song->artist,
					2, data->song->album,
					3, data->song->file,-1);
		}
	}
	if(gtk_tree_model_iter_n_children(GTK_TREE_MODEL(rs_store), NULL) >0)
	{
		gtk_widget_set_sensitive(glade_xml_get_widget(apl_xml, "okbutton1"), TRUE);
	}
	else{
		gtk_widget_set_sensitive(glade_xml_get_widget(apl_xml, "okbutton1"), FALSE);
	}
}

static void apl_response(GtkWidget *dialog, gint response, GladeXML *apl_xml)
{
	if(response == GTK_RESPONSE_OK)
	{
		GtkTreeIter iter;
		GtkWidget *tree = glade_xml_get_widget(apl_xml, "result_tree");
		GtkListStore *rs_store =(GtkListStore *)gtk_tree_view_get_model(GTK_TREE_VIEW(tree));
		
		if(!gtk_toggle_button_get_active
				(GTK_TOGGLE_BUTTON(glade_xml_get_widget(apl_xml, "ck_append_playlist"))))
		{
			mpd_playlist_clear(connection);
		}
		if(gtk_tree_model_get_iter_first(GTK_TREE_MODEL(rs_store), &iter))
		{
			do{
				char *path;
				gtk_tree_model_get(GTK_TREE_MODEL(rs_store), &iter, 3, &path, -1);
				mpd_playlist_queue_add(connection, path);

			}while(gtk_tree_model_iter_next(GTK_TREE_MODEL(rs_store), &iter));
			mpd_playlist_queue_commit(connection);
		}

	}

	apl_close(dialog, apl_xml);
}


static void field_combo_edited_cb (GtkCellRendererText *cell,
		const gchar *path,
		const gchar *value,
		GtkListStore *list)
{
	GtkTreeIter iter;
	if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(list), &iter, path))
	{
		gtk_list_store_set(list, &iter,
				0,value,
				-1);
	}
}

static void rule_combo_edited_cb (GtkCellRendererText *cell,
		const gchar *path,
		const gchar *value,
		GtkListStore *list)
{
	GtkTreeIter iter;
	if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(list), &iter, path))
	{
		gtk_list_store_set(list, &iter, 1,value, -1);
	}
}
static void text_edited_cb (GtkCellRendererText *cell,
		const gchar *path,
		const gchar *value,
		GtkListStore *list)
{
	GtkTreeIter iter;
	if(gtk_tree_model_get_iter_from_string(GTK_TREE_MODEL(list), &iter, path))
	{
		gtk_list_store_set(list, &iter, 2,value, -1);
	}
}
static void apl_add_row(GtkWidget *button, GladeXML *apl_xml)
{
	GtkTreeIter iter;
	GtkListStore *lstore = (GtkListStore *)gtk_tree_view_get_model(
			GTK_TREE_VIEW (glade_xml_get_widget(apl_xml, "apl_tree")));
	gtk_list_store_append(lstore, &iter);
	gtk_list_store_set(lstore, &iter,
			0, "Artist",
			1,"Contains",
			2,"<Value>",
			3, TRUE,
			-1);
}

static void apl_remove_row(GtkWidget *button, GladeXML *apl_xml)
{
	GtkTreeIter iter;
	GtkTreeView *tree = GTK_TREE_VIEW (glade_xml_get_widget(apl_xml, "apl_tree"));
	GtkTreeModel *lstore = gtk_tree_view_get_model(tree);

	if(gtk_tree_selection_get_selected(gtk_tree_view_get_selection(tree),&lstore,&iter))
	{
		gtk_list_store_remove(GTK_LIST_STORE(lstore), &iter);
	}
}
static void apl_start()
{

	int i=0, max = 3;
	GtkCellRenderer *renderer;
	GtkTreeIter iter;
	GtkTreeViewColumn *column;
	GtkWidget *tree = NULL;
	GtkListStore *lstore = NULL, *tmp_store = NULL;
	gchar *path = g_strdup_printf("%s/apl.glade",DATA_DIR);
	apl_xml = glade_xml_new(path, "apl_win", NULL);
	g_free(path);
	if(apl_xml == NULL)
	{
		debug_printf(DEBUG_ERROR, "apl_start: Cannot find: %s/apl.glade", DATA_DIR);
		return;
	}
	debug_printf(DEBUG_INFO, "apl_start: Starting");
	lstore = gtk_list_store_new(4, G_TYPE_STRING,G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN);
	gtk_tree_view_set_model(GTK_TREE_VIEW (glade_xml_get_widget(apl_xml, "apl_tree")), GTK_TREE_MODEL(lstore));
	g_object_unref(lstore);
	/* need 3 columns */
	/* combo box */
	renderer = gtk_cell_renderer_combo_new();
	column = gtk_tree_view_column_new();
	gtk_tree_view_column_pack_start(column, renderer, TRUE);
	gtk_tree_view_column_set_attributes(column, renderer, "text", 0,NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (glade_xml_get_widget(apl_xml, "apl_tree")), column);
	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
	tmp_store = gtk_list_store_new(1, G_TYPE_STRING);

	if(mpd_server_check_version(connection,0,12,0))
	{
		max = MPD_TAG_NUM_OF_ITEM_TYPES;
	}
	for(i=0;i< max;i++)
	{
		if(i != MPD_TAG_ITEM_COMMENT) /* I Don't want COMMENT */
		{
			gtk_list_store_append(tmp_store, &iter);
			gtk_list_store_set(tmp_store,&iter, 0, mpdTagItemKeys[i],-1);
		}
	}
	g_object_set(G_OBJECT(renderer), "model", tmp_store, NULL);
	g_object_set(G_OBJECT(renderer), "text-column", 0, NULL);
	g_object_set(G_OBJECT(renderer), "has-entry", FALSE, NULL);
	g_object_set(G_OBJECT(renderer), "editable", TRUE, NULL);
	g_signal_connect(G_OBJECT(renderer), "edited", G_CALLBACK(field_combo_edited_cb), lstore);


	/* combo box */
	renderer = gtk_cell_renderer_combo_new();
	column = gtk_tree_view_column_new();
	gtk_tree_view_column_pack_start(column, renderer, TRUE);
	gtk_tree_view_column_set_attributes(column, renderer, "text", 1,NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (glade_xml_get_widget(apl_xml, "apl_tree")), column);
	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
	tmp_store = gtk_list_store_new(1, G_TYPE_STRING);
	gtk_list_store_append(tmp_store, &iter);
	gtk_list_store_set(tmp_store, &iter, 0, "Contains",-1);
	gtk_list_store_append(tmp_store, &iter);
	gtk_list_store_set(tmp_store, &iter, 0, "Does not contain",-1);
	gtk_list_store_append(tmp_store, &iter);
	gtk_list_store_set(tmp_store, &iter, 0, "Equals",-1);
	g_object_set(G_OBJECT(renderer), "model", tmp_store, NULL);
	g_object_set(G_OBJECT(renderer), "text-column", 0, NULL);
	g_object_set(G_OBJECT(renderer), "has-entry", FALSE, NULL);
	g_object_set(G_OBJECT(renderer), "editable", TRUE, NULL);
	g_signal_connect(G_OBJECT(renderer), "edited", G_CALLBACK(rule_combo_edited_cb), lstore);


	/* text */
	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new();
	gtk_tree_view_column_pack_start(column, renderer, TRUE);
	gtk_tree_view_column_set_attributes(column, renderer, "text", 2,"editable", 3,NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (glade_xml_get_widget(apl_xml, "apl_tree")), column);
	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
	g_signal_connect(G_OBJECT(renderer), "edited", G_CALLBACK(text_edited_cb), lstore);


	gtk_list_store_append(lstore, &iter);
	gtk_list_store_set(lstore, &iter,
			0, "Artist",
			1,"Contains",
			2,"<Value>",
			3, TRUE,
			-1);


	/** Result */
	tree = glade_xml_get_widget(apl_xml, "result_tree");
	lstore = gtk_list_store_new(4, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	gtk_tree_view_set_model(GTK_TREE_VIEW(tree), GTK_TREE_MODEL(lstore));
	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),-1,"Title", renderer,
			"text", 0,NULL);
	gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),-1,"Artist", renderer,
			"text", 1,NULL);                                                      	
	renderer = gtk_cell_renderer_text_new();                                              		
	gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(tree),-1,"Album", renderer,
			"text", 2,NULL);

	glade_xml_signal_connect_data (apl_xml, "apl_search",
			G_CALLBACK(apl_search), apl_xml);


	glade_xml_signal_connect_data (apl_xml, "on_apl_win_close",
			G_CALLBACK(apl_close), apl_xml);
	glade_xml_signal_connect_data (apl_xml, "on_apl_win_response",
			G_CALLBACK(apl_response), apl_xml);
	glade_xml_signal_connect_data (apl_xml, "on_apl_add_clicked",
			G_CALLBACK(apl_add_row), apl_xml);
	glade_xml_signal_connect_data (apl_xml, "on_apl_remove_clicked",
			G_CALLBACK(apl_remove_row), apl_xml);
}



int apl_right_mouse_menu(GtkWidget *menu, int type, GtkWidget *tree, GdkEventButton *event)
{
	gmpcPlugin *plug = plugin_get_from_id(type);
	if(!cfg_get_single_value_as_int_with_default(config, "autoplaylist", "enable", TRUE)) {
		return 0;
	}
	debug_printf(DEBUG_INFO,"Automatic playlist right mouse clicked");	
	if(!strcmp(plug->name, "Current Playlist Browser") && mpd_server_check_version(connection, 0,12,0))
	{
		GtkWidget *item;
		debug_printf(DEBUG_INFO,"Automatic playlist right mouse for me");	
		item = gtk_image_menu_item_new_with_label("Generate Playlist");
		gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item),
				gtk_image_new_from_stock(GTK_STOCK_ADD, GTK_ICON_SIZE_MENU));
		gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);
		g_signal_connect(G_OBJECT(item), "activate", G_CALLBACK(apl_start), NULL);
		return 1;
	}
	return 0;
}

/**
 * e_util_unicode_get_utf8:
 * @text: The string to take the UTF-8 character from.
 * @out: The location to store the UTF-8 character in.
 *
 * Get a UTF-8 character from the beginning of @text.
 *
 * Returns: A pointer to the next character in @text after @out.
 **/
static guchar * e_util_unicode_get_utf8 (const gchar *text, gunichar *out)
{
	*out = g_utf8_get_char (text);
	return (guchar *)((*out == (gunichar)-1) ? NULL : g_utf8_next_char (text));
}


/**
 * e_util_utf8_strstrcase:
 * @haystack: The string to search in.
 * @needle: The string to search for.
 *
 * Find the first instance of @needle in @haystack, ignoring case. (No
 * proper case folding or decomposing is done.) Both @needle and
 * @haystack are UTF-8 strings.
 *
 * Returns: A pointer to the first instance of @needle in @haystack, or
 *          %NULL if no match is found, or if either of the strings are
 *          not legal UTF-8 strings.
 **/
int e_util_utf8_strstrcase (const gchar *haystack, const gchar *needle)
{
	gunichar *nuni;
	gunichar unival;
	gint nlen;
	const guchar *o, *p;

	if (haystack == NULL) return FALSE;
	if (needle == NULL) return FALSE;
	if (strlen (needle) == 0) return FALSE;
	if (strlen (haystack) == 0) return FALSE;

	nuni = g_alloca (sizeof (gunichar) * strlen (needle));

	nlen = 0;
	for (p = e_util_unicode_get_utf8 (needle, &unival);
			p && unival;
			p = e_util_unicode_get_utf8 ((const gchar *)p, &unival))
	{
		nuni[nlen++] = g_unichar_tolower (unival);
	}
	/* NULL means there was illegal utf-8 sequence */
	if (!p){

		return FALSE;
	}


	o = (const guchar *)haystack;
	for (p = e_util_unicode_get_utf8 ((const gchar *)o, &unival);
			p && unival;
			p = e_util_unicode_get_utf8 ((const gchar *)p, &unival))
	{
		gint sc;
		sc = g_unichar_tolower (unival);
		/* We have valid stripped char */
		if (sc == nuni[0]) {
			const guchar *q = p;
			gint npos = 1;
			while (npos < nlen) {
				q = e_util_unicode_get_utf8 ((const gchar *)q, &unival);
				if (!q || !unival) return FALSE;
				sc = g_unichar_tolower (unival);
				if (sc != nuni[npos]) break;
				npos++;
			}
			if (npos == nlen) {
				return TRUE;
			}
		}
		o = p;
	}
	return FALSE;
}
